const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const mysql = require('mysql2/promise')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

const instructorModel = require('./models/instructor')
const courseModel = require('./models/course')
const instructorCon = require('./controllers/instructor')
const courseCon = require('./controllers/course')

const pool = mysql.createPool({
  connectionLimit: 5,
  host: 'localhost',
  user: 'root',
  database: 'codecamp2'
})

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})

const instructorController = instructorCon(instructorModel, pool)
const courseController = courseCon(courseModel, pool)

router.get('/instructor/find_all', instructorController.findInstructorAll)

router.get('/instructor/find_by_id/:id', instructorController.findInstructorById)

router.get('/course/find_by_id/:id', courseController.findCourseById)

router.get('/course/find_by_price/:price', courseController.findCourseByPrice)

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
