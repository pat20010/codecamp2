'use strict'

module.exports = function (instructorModel, pool) {
  return {
    async findInstructorAll (ctx, next) {
      let data = await instructorModel.findAll(pool)
      console.log(data)
      await ctx.render('tableInstructor', {
        rowData: data
      })
      await next()
    },

    async findInstructorById (ctx, next) {
      let reqId = ctx.params.id
      let data = await instructorModel.findById(pool, reqId)

      await ctx.render('tableInstructor', {
        rowData: data
      })
      await next()
    }
  }
}
