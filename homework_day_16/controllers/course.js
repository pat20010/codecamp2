'use strict'

module.exports = function (courseModel, pool) {
  return {
    async findCourseById (ctx) {
      let reqId = ctx.params.id
      const data = await courseModel.findById(pool, reqId)

      await ctx.render('tableCourse', {
        rowData: data
      })
    },

    async findCourseByPrice (ctx) {
      let reqId = ctx.params.price
      const data = await courseModel.findByPrice(pool, reqId)

      await ctx.render('tableCourse', {
        rowData: data
      })
    }
  }
}
