'use strict'
module.exports = {
  async createEntity (row) {
    if (!row.id) { return {} }

    return {
      id: row.id,
      name: row.name
    }
  },

  async findAll (pool) {
    const [rows] = await pool.query(`SELECT * FROM instructors`)
    return rows
  },

  async findById (pool, id) {
    const [rows] = await pool.query(`SELECT * FROM instructors WHERE id = ?`, [id])
    return rows
  },

  async removeById (pool, id) {
    await pool.query(`DELETE FROM instructors WHERE id = ?`, [id])
    return 1
  },

  async insert (pool, instructorObj) {
    await pool.query(`INSERT INTO instructors (id, name, created_at) VALUES (?, ?, now())`, [instructorObj.id, instructorObj.name])
    return instructorObj
  }
}
