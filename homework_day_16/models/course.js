'use strict'
module.exports = {
  async createEntity (row) {
    if (!row.id) { return {} }

    return {
      id: row.id,
      name: row.name,
      detail: row.detail,
      price: row.price,
      teach_by: row.teach_by,
      created_at: row.created_at
    }
  },

  async findAll (pool) {
    const [rows] = await pool.query(`SELECT * FROM courses`)
    return rows
  },

  async findById (pool, id) {
    const [rows] = await pool.query(`SELECT * FROM courses WHERE id = ?`, [id])
    console.log(rows)
    return rows
  },

  async findByPrice (pool, price) {
    const [rows] = await pool.query(`SELECT * FROM courses WHERE price = ?`, [price])
    console.log(rows)
    return rows
  },

  async removeById (pool, id) {
    await pool.query(`DELETE FROM courses WHERE id = ?`, [id])
    return 1
  },

  async insert (pool, courseObj) {
    await pool.query(`INSERT INTO courses (id, name, detail, price, teach_by, created_at) VALUES (?, ?, ?, ?, ?, now())`, [courseObj.id, courseObj.name, courseObj.detail, courseObj.price, courseObj.teach_by])
    return courseObj
  }
}
