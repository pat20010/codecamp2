const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

const pool = mysql.createPool({
  connectionLimit: 5,
  host: 'localhost',
  user: 'root',
  database: 'codecamp2'
})

router.get('/instructor/find_all', async (ctx, next) => {
  const [rowsInstructor] = await pool.query(`SELECT * FROM instructors`)

  ctx.body = rowsInstructor
})

router.get('/instructor/find_by_id/:id', async (ctx, next) => {
  let reqId = ctx.params.id
  const [rowsInstructor] = await pool.query(`SELECT * FROM instructors WHERE id = ?`, [reqId])

  ctx.body = rowsInstructor
})

router.get('/course/find_by_id/:id', async (ctx, next) => {
  let reqId = ctx.params.id
  const [rowsCourse] = await pool.query(`SELECT * FROM courses WHERE id = ?`, [reqId])

  ctx.body = rowsCourse
})

router.get('/course/find_by_price/:price', async (ctx, next) => {
  let reqId = ctx.params.price
  const [rowsCourse] = await pool.query(`SELECT * FROM courses WHERE price = ?`, [reqId])

  ctx.body = rowsCourse
})

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
