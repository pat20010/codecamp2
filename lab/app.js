"use strict";

const fs = require("fs");

// fs.writeFile("demo_wirte_file1.txt", "Test write demo file", "utf-8", err => {
//   fs.readFile("demo_wirte_file1.txt", "utf8", (err, data) => {
//     console.log(`Read data : ${data}`);
//   });
// });

let callbackValue;
function hello4(err, value) {
  callbackValue = value;
}

function tryHello4(callbackFunction) {
  let returnValue = 'returnValue';
  let callbackValue = 'callbackValue';
  callbackFunction(null, callbackValue);
  return returnValue;
}

console.log(tryHello4(hello4)); // print returnValue
console.log(callbackValue); // print callbackValue


// let people = ["a1", "a2"];

// let people2 = people.map(name => {
//   return `Mr. ${name}`;
// });

// let data = [
//   {
//     country: "China",
//     pop: 1409517397
//   },
//   {
//     country: "India",
//     pop: 1339180127
//   },
//   {
//     country: "USA",
//     pop: 324459463
//   },
//   {
//     country: "Indonesia",
//     pop: 263991379
//   }
// ];

// let sum = data.reduce((acc, val) => {
//   console.log(acc);
//   return val.country == "China" ? acc : acc + val.pop;
// }, 0);

// console.log(people);

// console.log(people2);

// console.log(sum);
