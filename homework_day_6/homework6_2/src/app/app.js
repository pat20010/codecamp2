import "../styles/styles.css";
import $ from "jquery";
import "popper.js";
import "bootstrap-select";
import "bootstrap";

$(function() {
  $(".selectpicker").selectpicker({
    style: "btn-info",
    size: 4
  });
});
