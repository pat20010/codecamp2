import "../styles/styles.css";
import $ from "jquery";

$("#select_theme").change(function() {
  console.log($("#select_theme").val());

  switch ($("#select_theme").val()) {
    case "1":
      $("#subtitle").attr("class", "subtitle1");
      $("#detail").attr("class", "text-justify1");
      break;
    case "2":
      $("#subtitle").attr("class", "subtitle2");
      $("#detail").attr("class", "text-justify2");
      break;
    case "3":
      $("#subtitle").attr("class", "subtitle3");
      $("#detail").attr("class", "text-justify3");
      break;
  }
});
