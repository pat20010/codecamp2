const mysql = require('mysql2/promise')
const assert = require('assert')
const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  database: 'koa_login'
})

const tempUserController = require('../controller/user')
const userController = tempUserController(null, pool)

const ctx = {
  request: {
    body: {
      username: 'test1',
      password: '1234',
      email: 'test@test.com'
    }
  },
  async render () {

  }
}

async function next () {
  return true
}

describe('userController', function () {
  describe('insertUser()', function () {
    it('should return with empty errorMessage', async function () {
      const resData = await userController.insertUser(ctx, next)

      assert.equal(resData.errorMessage, '')
    })
  })
})

describe('userController', function () {
  describe('insertUser()', function () {
    it('should be equal to this object structure', async function () {
      const resData = await userController.insertUser(ctx, next)

      assert.deepEqual(resData, {
        errorMessage: '',
        userId: '123',
        username: 'test1' })
    })
  })
})
