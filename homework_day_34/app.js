const Koa = require('Koa')
const app = new Koa()
const Router = require('koa-router')
const router = new Router()
const render = require('koa-ejs')
const path = require('path')
const koaBody = require('koa-body')
const session = require('koa-session')
const mysql = require('mysql2/promise')
const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  database: 'koa_login'
})

app.use(koaBody())
app.keys = ['some sadfk dsjkl fjsdlak;j fklsdj lkjfklsdjf ldks;ssdfs']
const sessionStore = {}

const CONFIG = {
  key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
  maxAge: 86400000 * 7,
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false) */
  store: {
    get (key, maxAge, { rolling }) {
      return sessionStore[key]
    },
    set (key, sess, maxAge, { rolling, changed }) {
      sessionStore[key] = sess
    },
    destroy (key) {
      delete sessionStore[key]
    }
  }
}

app.use(session(CONFIG, app))

render(app, {
  root: path.join(__dirname, 'view'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})

async function usernameExisted (username) {
  const [usernameQuery] = await pool.query(`
        SELECT username FROM user
        WHERE username = ?
    `, [username])
  if (usernameQuery[0].username) {
    return true
  } else {
    return false
  }
}

async function emailExisted (email) {
  const [emailQuery] = await pool.query(`
        SELECT email FROM user
        WHERE email = ?
    `, [email])

  if (emailQuery[0].email) {
    return true
  } else {
    return false
  }
}
async function insertUser (pool, username, password, email) {
  const aData = [
    username,
    this.stupidHash(password),
    email
  ]
  const sql = `INSERT INTO user (username, password, email) VALUES (?, ? ,?)`
  const [resultInsert] = await pool.query(sql, aData)

  return resultInsert.insertId
}
async function getUserInfo (pool, userId) {
  const [results] = await pool.query(`
            SELECT id, email 
            FROM user 
            WHERE id = ?
        `, [userId])

  return {
    userId: results[0].userId,
    email: results[0].email
  }
}
function stupidHash (password) {
  return '123' + password + '123'
}

router.get('/', async (ctx, next) => {
  await ctx.render('register')
  await next()
})

router.get('/register_ajax', async (ctx, next) => {
  await ctx.render('register_ajax')
  await next()
})

router.get('/login', async (ctx, next) => {
  await ctx.render('login')
  await next()
})

router.post('/register_completed', async (ctx, next) => {
  let errorMessage = ''

  /*
    if (usernameExisted(ctx.request.body.username)) {
        errorMessage += "Username already existed!<br>";
    }
    if (emailExisted(ctx.request.body.email)) {
        errorMessage += "Email already existed!<br>";
    }
    */

  const dataSend = {errorMessage: errorMessage}
  if (errorMessage == '') {
    const sql = `INSERT INTO user (username, password, email) VALUES (?, ? ,?)`
    const [resultInsert] = await pool.query(sql, [
      username,
      stupidHash(password),
      email
    ])
    dataSend.userId = resultInsert.insertId
    dataSend.username = ctx.request.body.username
  }

  await ctx.render('register_result', dataSend)
  await next()
})

router.post('/register_completed_ajax', async (ctx, next) => {
  let errorMessage = ''

  /*
    if (await usernameExisted(ctx.request.body.username)) {
        errorMessage += "Username already existed!<br>";
    }
    if (await emailExisted(ctx.request.body.email)) {
        errorMessage += "Email already existed!<br>";
    }
    */

  const dataSend = {errorMessage: errorMessage}
  if (errorMessage == '') {
    const sql = `INSERT INTO user (username, password, email) VALUES (?, ? ,?)`
    const [resultInsert] = await pool.query(sql, [
      username,
      stupidHash(password),
      email
    ])
    dataSend.userId = resultInsert.insertId
    dataSend.username = ctx.request.body.username
  }

  ctx.body = dataSend

  await next()
})

router.post('/login_completed', async (ctx, next) => {
  const [results, fields] = await pool.query(`
        SELECT id, password 
        FROM user 
        WHERE username = ?
    `, [ctx.request.body.username])
  if (results[0].password == stupidHash(ctx.request.body.password)) {
    ctx.session.userId = results[0].id
    ctx.redirect('/profile')
  } else {
    ctx.body = 'Login fail'
  }
  await next()
})

router.get('/profile', async (ctx, next) => {
  const userId = ctx.params.id ? ctx.params.id : ctx.session.userId
  const data = await getUserInfo(pool, userId)

  await ctx.render('profile', {
    userId: data.userId,
    email: data.email
  })
  await next()
})

router.get('/profile/:id', async (ctx, next) => {
  const userId = ctx.params.id ? ctx.params.id : ctx.session.userId
  const data = await getUserInfo(pool, userId)

  await ctx.render('profile', {
    userId: data.userId,
    email: data.email
  })
  await next()
})

router.get('/logout', async (ctx, next) => {
  ctx.session = null

  await ctx.render('logout')
  await next()
})

app.use(async (ctx, next) => {
  const urlAllowWithoutLogin = ['/', '/login', '/login_completed', '/register_ajax', '/register_completed', '/register_completed_ajax', '/logout']
  if (urlAllowWithoutLogin.indexOf(ctx.path) === -1) {
    // Check if user has already logged in
    if (ctx.session && ctx.session.userId) {
      await next() // can access
    } else {
      await ctx.render('login') // force to login if there's no userId
    }
  } else {
    await next()
  }
})
app.use(router.routes())
app.listen(3000)
