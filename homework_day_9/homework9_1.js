'use strict';

const fs = require('fs');

async function writeRobot() {
    let head = await readFile('../homework_day_8/homework8_1/head.txt');
    console.log(head);
    let body = await readFile('../homework_day_8/homework8_1/body.txt');
    console.log(body);
    let leg = await readFile('../homework_day_8/homework8_1/leg.txt');
    console.log(leg);
    let feet = await readFile('../homework_day_8/homework8_1/feet.txt');
    console.log(feet);
    writeFile('Robot.txt', `${head}\n${body}\n${leg}\n${feet}`);
}
writeRobot();

function readFile(directory) {

    return new Promise((resolve, reject) => {
        fs.readFile(directory, 'utf-8', function (err, data) {
            if (err) {
                reject(`Error : ${err}`);
            }
            return resolve(data);
        });
    });
}

function writeFile(fileName, content) {

    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, content, 'utf-8', function (err) {
            if (err) {
                reject(`Error : ${err}`);
            }
        });
    });
}