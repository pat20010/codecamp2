'use strict';

const fs = require('fs');

let arrFunc = [];
arrFunc.push(readFile('../homework_day_8/homework8_1/head.txt'));
arrFunc.push(readFile('../homework_day_8/homework8_1/body.txt'));
arrFunc.push(readFile('../homework_day_8/homework8_1/leg.txt'));
arrFunc.push(readFile('../homework_day_8/homework8_1/feet.txt'));

async function responsePromse() {

    let result = await Promise.all(arrFunc);

    let content = result.reduce((text, res) => {
        return text += `\n${res}`;
    });

    writeFile('Robot2.txt', content);
}
responsePromse();

function readFile(directory) {

    return new Promise((resolve, reject) => {
        fs.readFile(directory, 'utf-8', function (err, data) {
            if (err) {
                reject(`Error : ${err}`);
            }
            return resolve(data);
        });
    });
}

function writeFile(fileName, content) {

    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, content, 'utf-8', function (err) {
            if (err) {
                reject(`Error : ${err}`);
            }
        });
    });
}