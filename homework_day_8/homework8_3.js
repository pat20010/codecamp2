function getDataTable() {

    fetch('homework2_1.json').then(res => {
        return res.json();
    }).then(responseJson => {
        const resObj = responseJson;

        for (let index in resObj) {

            let textElement = "";
            for (let keyName in resObj[index]) {

                if (index == 0) {
                    $('#myTable').append($('<th>' + keyName + '</th>'));
                }
                keyID = resObj[index][keyName] + " ";

                textElement += '<td>' + keyID + '</td>';
            }
            $('#myTable').append($('<tr>' + textElement + '</tr>'));
        }

        updateLoadPercentData();
    }).catch(err => {
        console.log(`Error : ${err}`);
    });
}

let percent = 0;

$(function () {
    srcImage1 = 'http://www.effigis.com/wp-content/uploads/2015/02/Iunctus_SPOT5_5m_8bit_RGB_DRA_torngat_mountains_national_park_8bits_1.jpg';
    srcImage2 = "http://www.effigis.com/wp-content/uploads/2015/02/Infoterra_Terrasar-X_1_75_m_Radar_2007DEC15_Toronto_EEC-RE_8bits_sub_r_12.jpg";
    srcImage3 = "http://www.effigis.com/wp-content/uploads/2015/02/DigitalGlobe_WorldView1_50cm_8bit_BW_DRA_Bangkok_Thailand_2009JAN06_8bits_sub_r_1.jpg";

    $('.modal').css('display', 'block');

    $('#myTable').ready(getDataTable());

    $('#image1')
        .ready(updateLoadPercentImage('#image1', srcImage1));

    $('#image2')
        .ready(updateLoadPercentImage('#image2', srcImage2));

    $('#image3')
        .ready(updateLoadPercentImage('#image3', srcImage3));
});

updateLoadPercentImage = (id, image) => {

    isImageLoaded(image, function () {
        $('#progressLoad').attr('style', `width:${percent += 25}%;`);
        $('#progressLoad').text(`${percent}%`);
        $(id).attr('src', image);

        if (percent === 100) {
            setTimeout(() => {
                $('.modal').css('display', 'none');
            }, 2000);
        }
    });
}

isImageLoaded = (image, callbackLoaded) => {
    objImg = new Image();
    objImg.src = image;
    objImg.onload = callbackLoaded;
}

updateLoadPercentData = () => {
    $('#progressLoad').attr('style', `width:${percent += 25}%;`);
    $('#progressLoad').text(`${percent}%`);

    if (percent === 100) {
        setTimeout(() => {
            $('.modal').css('display', 'none');
        }, 2000);
    }
}