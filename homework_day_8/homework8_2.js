'use strict';

const fs = require('fs');

let dataHead = '';
let dataBody = '';
let dataLeg = '';
let dataFeet = '';

fs.readFile('homework8_1/head.txt', 'utf-8', readHeadFile);

function readHeadFile(err, data1) {
    if (err) {
        console.log(`Error : ${err}`);
        return;
    }

    dataHead = data1;
    fs.readFile('homework8_1/body.txt', 'utf-8', readBodyFile);
}

function readBodyFile(err, data2) {
    if (err) {
        console.log(`Error : ${err}`);
        return;
    }

    dataBody = data2;
    fs.readFile('homework8_1/leg.txt', 'utf-8', readlegFile);
}

function readlegFile(err, data3) {
    if (err) {
        console.log(`Error : ${err}`);
        return;
    }

    dataLeg = data3;
    fs.readFile('homework8_1/feet.txt', 'utf-8', readFeetFile);
}

function readFeetFile(err, data4) {
    if (err) {
        console.log(`Error : ${err}`);
        return;
    }

    dataFeet = data4;
    fs.writeFile('robot2.txt', `${dataHead}\n${dataBody}\n${dataLeg}\n${dataFeet}`, 'utf-8', writeRobotFile);
}

function writeRobotFile(err) {
    if (err) {
        console.log(`Error : ${err}`);
        return;
    }
}