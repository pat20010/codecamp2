
'use strict'
const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    connectionLimit: 5,
    host: 'localhost',
    user: 'root',
    database: 'db1'
})

module.exports.pool = pool
module.exports.mysql = mysql