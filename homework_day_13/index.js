'use strict'
const Koa = require('koa')
const Router = require('koa-router')
const { pool } = require('./config/connectDB')
const path = require('path')
const serve = require('koa-static')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})

router.get('/', async (ctx, next) => {
  const queryTeachNoCoure = 'Select i.name as instructor_name, c.name From instructors as i Left join courses as c on i.id = c.teach_by Where c.name is null'
  const queryCoureNoTeach = 'Select c.name as coures_name From instructors as i Right join courses as c on i.id = c.teach_by Where teach_by is null'

  const [rowsTeach] = await pool.query(queryTeachNoCoure)
  const [rowsCoure] = await pool.query(queryCoureNoTeach)

  await ctx.render('index', {
    teachs: rowsTeach,
    coures: rowsCoure
  })
})

router.get('/search', async (ctx, next) => {
  let queryString = ''

  if (ctx.request.query.search_course) {
    queryString = ctx.request.query.search_course
  }

  const queryCourse = `Select c.id, c.name as course_name, c.price, i.name as teach_name From Courses c Left join instructors i On c.teach_by = i.id Where c.name like ?`
  const [rowsCourse] = await pool.query(queryCourse, [`%${queryString}%`])

  console.log(rowsCourse)

  await ctx.render('search', {
    dataCourse: rowsCourse
  })
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
