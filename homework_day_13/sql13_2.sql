INSERT INTO students
    (name)
VALUES
    ('Mike'),
    ('Susie'),
    ('Max'),
    ('Nile'),
    ('Mike'),
    ('Cohen'),
    ('Naja'),
    ('Henry'),
    ('Susie'),
    ('Kylian');


INSERT INTO enrolls
    (student_id, course_id)
VALUES
    ('1', '5'),
    ('2', '21'),
    ('3', '7'),
    ('4', '1'),
    ('5', '15'),
    ('6', '10'),
    ('7', '8'),
    ('8', '22'),
    ('9', '11'),
    ('10', '20');


SELECT c.name as course_name
FROM courses c INNER JOIN enrolls e ON c.id = e.course_id;


SELECT c.name as course_name
FROM courses c LEFT JOIN enrolls e ON c.id = e.course_id WHERE e.student_id IS NULL;