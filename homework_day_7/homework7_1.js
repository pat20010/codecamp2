let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function modulo2(number) {
  let num = number % 2;
  if (num === 0) {
    return 1;
  } else {
    return 0;
  }
}

function multiply1000(number) {
  return number * 1000;
}

let resultArr = arr.filter(modulo2).map(multiply1000);

console.log(resultArr);
