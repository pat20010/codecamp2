"use strict";

fetch("/homework_day_2/homework2_1.json")
  .then(response => {
    return response.json();
  })
  .then(responseJson => {
    console.log(responseJson);

    const peopleSalary = responseJson;

    const peopleLowSalary = peopleSalary
      .filter(getPeopleSalaryLessThen)
      .map(upSalaryDouble);

    console.log(peopleLowSalary);

    const sumSalary = peopleSalary.map(peopleSalaryLessThen);

    console.log(sumSalary);
  })
  .catch(err => {
    console.log(`Error : ${err}`);
  });

let getPeopleSalaryLessThen = x => x.salary < 100000;

let upSalaryDouble = peopleSalary => {
  let newPeopleSalary = {};
  newPeopleSalary.id = peopleSalary.id;
  newPeopleSalary.firstname = peopleSalary.firstname;
  newPeopleSalary.lastname = peopleSalary.lastname;
  newPeopleSalary.company = peopleSalary.company;
  newPeopleSalary.salary = peopleSalary.salary * 2;
  return newPeopleSalary;
};

let peopleSalaryLessThen = peopleSalary => {
  let newPeopleSalary = {};
  newPeopleSalary.id = peopleSalary.id;
  newPeopleSalary.firstname = peopleSalary.firstname;
  newPeopleSalary.lastname = peopleSalary.lastname;
  newPeopleSalary.company = peopleSalary.company;
  newPeopleSalary.salary = peopleSalary.salary;
  if (newPeopleSalary.salary < 100000) {
    newPeopleSalary.salary = newPeopleSalary.salary * 2;
  }
  return newPeopleSalary;
};

// fetch("/homework_day_2/homework2_1.json")
//   .then(response => {
//     return response.json();
//   })
//   .then(responseJson => {
//     console.log(responseJson);
//     let newArr = responseJson.map(i => {
//       return i.salary < 100000 ? i.salary * 2 : i.salary;
//     });
//     console.log(newArr);
//     console.log(responseJson);
//   });
