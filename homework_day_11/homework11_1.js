'use strict'
const Koa = require('koa')
const { router, render } = require('./routes/router')
const serve = require('koa-static')
const path = require('path')

const app = new Koa()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework11_1',
    viewExt: 'ejs',
    cache: false
})

async function middleware(ctx, next) {
    let date = new Date()
    let cuerentDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
    let currentTime = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
    let currentDateTime = `${cuerentDate} ${currentTime}`

    ctx.body.additionalData = { userId: 1, dateTime: currentDateTime }
}

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.use(middleware)
app.listen(3000)