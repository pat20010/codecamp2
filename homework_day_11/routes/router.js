'use strict'
const Router = require('koa-router')
const render = require('koa-ejs')
const { pool } = require('../configs/connectDB')
const fs = require('fs')

const router = new Router()

router.get('/', async (ctx, next) => {
    const [rows, fields] = await pool.query('Select * From user')
    const key = Object.keys(rows[0])

    await ctx.render('homework11_1', {
        data: rows,
        key: key
    })
})

router.get('/from_database', async (ctx, next) => {
    const [rows, fields] = await pool.query('Select * From user')
    ctx.body = { data: rows }

    await next()
})

router.get('/from_file', async (ctx, next) => {
    const res = await new Promise((resolve, reject) => {
        fs.readFile('../homework_day_2/homework2_1.json', (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })

    const resJson = JSON.parse(res)
    ctx.body = { data: resJson }

    await next()
})

module.exports.router = router
module.exports.render = render