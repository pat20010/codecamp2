"use strict";

fetch("/homework_day_2/homework2_1.json")
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {
    const employees = responseJson;
    addAdditionalFields(employees);
  })
  .catch(function(error) {
    console.log("Error : " + error);
  });

function addYearSalary(row) {
  return (row.yearSalary = row.salary * 12);
}

function addNextSalary(row) {
  let year = 3;
  let salary = parseInt(row.salary);
  let arrSalary = [salary];
  for (let i = 0; i < year - 1; i++) {
    salary = arrSalary[i];
    arrSalary.push((salary += salary * 0.1));
  }
  return (row.nextSalary = arrSalary);
}

function addAdditionalFields(employees) {
  let textElement = "";
  for (let index in employees) {
    textElement = "";
    addYearSalary(employees[index]);
    addNextSalary(employees[index]);
    console.log(employees[index]);

    for (let key in employees[index]) {
      if (index == 0) {
        $("#tableEmployees").append($("<th>" + key + "</th>"));
      }
      if (key == "nextSalary") {
        let nextSalaryElement = "";
        employees[index][key].forEach(function(nextSalary) {
          nextSalaryElement += "<li>" + nextSalary + "</li>";
        });
        textElement += "<td><ol>" + nextSalaryElement + "</lo></td>";
      } else {
        textElement += "<td>" + employees[index][key] + "</td>";
      }
    }
    $("#tableEmployees").append($("<tr>" + textElement + "</tr>"));
  }
}
