"use strict";

fetch("/homework_day_2/homework2_1.json")
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {
    const employees = responseJson;
    let newEmployees = addAdditionalFields(employees);
    newEmployees[0].salary = 0;
    console.log(newEmployees);
    console.log(employees);
  })
  .catch(function(error) {
    console.log("Error : " + error);
  });

function addYearSalary(row) {
  return (row.yearSalary = row.salary * 12);
}

function addNextSalary(row) {
  let year = 3;
  let salary = parseInt(row.salary);
  let arrSalary = [salary];
  for (let i = 0; i < year - 1; i++) {
    salary = arrSalary[i];
    arrSalary.push((salary += salary * 0.1));
  }
  return (row.nextSalary = arrSalary);
}

function addAdditionalFields(employees) {
  let newEmployees = [];
  for (let index in employees) {
    let elementNewEmp = "";
    let elementEmp = "";
    let objEmployees = {};
    objEmployees.id = employees[index].id;
    objEmployees.firstname = employees[index].firstname;
    objEmployees.lastname = employees[index].lastname;
    objEmployees.company = employees[index].company;
    objEmployees.salary = employees[index].salary;
    addYearSalary(objEmployees);
    addNextSalary(objEmployees);
    newEmployees.push(objEmployees);

    for (let key in objEmployees) {
      if (index == 0) {
        $("#tableNewEmployees").append($("<th>" + key + "</th>"));
      }
      if (key == "nextSalary") {
        let nextSalaryElement = "";
        objEmployees[key].forEach(function(nextSalary) {
          nextSalaryElement += "<li>" + nextSalary + "</li>";
        });
        elementNewEmp += "<td><ol>" + nextSalaryElement + "</lo></td>";
      } else {
        elementNewEmp += "<td>" + objEmployees[key] + "</td>";
      }
    }
    $("#tableNewEmployees").append($("<tr>" + elementNewEmp + "</tr>"));

    for (let key in employees[index]) {
      if (index == 0) {
        $("#tableEmployees").append($("<th>" + key + "</th>"));
      }
      elementEmp += "<td>" + employees[index][key] + "</td>";
    }
    $("#tableEmployees").append($("<tr>" + elementEmp + "</tr>"));
  }
  return newEmployees;
}
