
SELECT e.student_id, s.name, COUNT(*) as total_course_enroll , SUM(c.price) as total_price_enroll
FROM enrolls e INNER JOIN students s ON s.id = e.student_id
    INNER JOIN courses c ON e.course_id = c.id
GROUP BY s.id;

SELECT e.student_id, s.name, MAX(c.price) as price_course, c.name
FROM enrolls e INNER JOIN students s ON s.id = e.student_id
    INNER JOIN courses c ON e.course_id = c.id
GROUP BY s.id;

SELECT e.student_id, s.name, AVG(c.price) as avg_price_course
FROM enrolls e INNER JOIN students s ON s.id = e.student_id
    INNER JOIN courses c ON e.course_id = c.id
GROUP BY s.id;