'use strict'
const Router = require('koa-router')
const { pool } = require('../configs/connectDB')
const render = require('koa-ejs')

const router = new Router()

router.get('/', async (ctx, next) => {
  const queryCourse = `SELECT c.id, c.name, sum(c.price) as price FROM Courses c INNER JOIN enrolls e ON c.id = e.course_id Group by c.id UNION
     SELECT '', 'Total price', sum(c.price) as total_price FROM Courses c INNER JOIN enrolls e ON c.id = e.course_id`
  const queryStudentEnroll = `SELECT s.id, s.name, COUNT(*) as total_course_enroll, SUM(c.price) as total_price FROM enrolls e 
     INNER JOIN students s ON s.id = e.student_id LEFT JOIN courses c ON c.id = e.course_id GROUP BY s.id`

  const [rowsCourse] = await pool.query(queryCourse)
  const [rowsStudent] = await pool.query(queryStudentEnroll)

  console.log(rowsCourse)
  console.log(rowsStudent)

  await ctx.render('index', {
    dataCourses: rowsCourse,
    dataStudents: rowsStudent
  })
})

module.exports.router = router
module.exports.render = render
