const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'data_table',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async ctx => {
    let res = await readJsonFile()
    let resJson = JSON.parse(res)
    let key = Object.keys(resJson[0])

    await ctx.render('data_table', {
        datas: resJson,
        keys: key
    });
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)

function readJsonFile() {

    return new Promise((resolve, reject) => {
        fs.readFile('../homework_day_2/homework2_1.json', (err, data) => {
            if (err) {
                reject(err)
            }
            resolve(data)
        })
    })
}