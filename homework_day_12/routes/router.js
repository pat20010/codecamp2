'use strict'
const Router = require('koa-router')
const { pool } = require('../configs/connectDB')
const dateFormat = require('dateformat')
const render = require('koa-ejs')
const fetch = require('isomorphic-unfetch')

const router = new Router()

router.get('/', async (ctx, next) => {
    const [rowsStaff, fieldsStaff] = await pool.query('Select * From staff')
    const [rowsBook, fieldsBook] = await pool.query('Select * From book')
    const keyStaff = Object.keys(rowsStaff[0])
    const keyBook = Object.keys(rowsBook[0])

    rowsStaff.forEach(rowData => {
        let createDate = rowData.create_datetime
        rowData.create_datetime = dateFormat(createDate, 'dd mmm yyyy HH:mm:ss')
    })

    rowsBook.forEach(rowData => {
        let createDate = rowData.create_datetime
        rowData.create_datetime = dateFormat(createDate, 'dd mmm yyyy HH:mm:ss')
    })

    await ctx.render('index', {
        dataStaff: rowsStaff,
        keyStaff: keyStaff,
        dataBook: rowsBook,
        keyBook: keyBook
    })
})

router.get('/add', async (ctx, next) => {
    const resObj = await (await fetch('https://randomuser.me/api/')).json()

    let firstName = resObj.results[0].name.first
    let lastName = resObj.results[0].name.last
    let birthDate = resObj.results[0].dob

    firstName = firstCharaterToUpperCase(firstName)
    lastName = firstCharaterToUpperCase(lastName)
    birthDate = calculateAge(new Date(birthDate))

    let stamentInsertStaff = `Insert Into staff (firstname, lastname, age) values ('${firstName}','${lastName}',${birthDate})`
    const [rowsIn, feildsIn] = await pool.query(stamentInsertStaff)

    if (rowsIn.affectedRows > 0) {
        ctx.status = 200
        ctx.body = {
            message: 'Add new staff success',
            id: `${rowsIn.insertId}`,
            name: `${firstName} ${lastName}`,
            age: `${birthDate}`
        }
    } else {
        ctx.status = 500
        ctx.body = {
            message: 'Add new staff fail'
        }
    }
})

const calculateAge = (birthDate) => {
    var dateDifMs = Date.now() - birthDate.getTime();
    var date = new Date(dateDifMs);
    return Math.abs(date.getUTCFullYear() - 1970);
}

const firstCharaterToUpperCase = (charater) => {
    return charater.charAt(0).toUpperCase() + charater.slice(1)
}

module.exports.router = router
module.exports.render = render