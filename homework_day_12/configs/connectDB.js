'use strict'
const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    connectionLimit: 5,
    host: 'localhost',
    user: 'root',
    database: 'book_store'
})

module.exports.pool = pool