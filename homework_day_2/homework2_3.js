function getDataIntoTable() {
  const peopleSalary = [
    {
      id: "1001",
      firstname: "Luke",
      lastname: "Skywalker",
      company: "Walt Disney",
      salary: "40000"
    },
    {
      id: "1002",
      firstname: "Tony",
      lastname: "Stark",
      company: "Marvel",
      salary: "1000000"
    },
    {
      id: "1003",
      firstname: "Somchai",
      lastname: "Jaidee",
      company: "Love2work",
      salary: "20000"
    },
    {
      id: "1004",
      firstname: "Monkey D",
      lastname: "Luffee",
      company: "One Piece",
      salary: "9000000"
    }
  ];

  for (let index in peopleSalary) {
    let textElement = "";
    for (let keyName in peopleSalary[index]) {
      
      if (keyName != "company") {
        if (index == 0) {
          $("#myTable").append($("<th>" + keyName + "</th>"));
        }

        if (keyName == "salary") {
          let arrSalary = [peopleSalary[index][keyName]];
          let year = 3;
          let percent = 0.1

          for (let indexSalary = 0; indexSalary < year - 1; indexSalary++) {
            let salary = parseInt(arrSalary[indexSalary]);
            salary += salary * percent;
            arrSalary.push(salary);
          }
          peopleSalary[index][keyName] = arrSalary;

          let salaryElement = "";
          for (let i = 0; i < arrSalary.length; i++) {
            salaryElement += "<li>" + arrSalary[i] + "</li>";
          }

          textElement += "<td><ol>" + salaryElement + "</ol></td>";
        } else {
          keyID = peopleSalary[index][keyName] + " ";

          textElement += "<td>" + keyID + "</td>";
        }
      }
    }
    $("#myTable").append($("<tr>" + textElement + "</tr>"));
  }
}
